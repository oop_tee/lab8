package pattarapol;

public class Circle {
    private String name;
    private double redise;

    public Circle(String name, double redise) {
        this.name = name;
        this.redise = redise;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public double circleArea() {
        double ra = (3.14) * (redise * redise);
        return ra;
    }

    public double circlePerimeter() {
        double pari = 2 * (3.14) * redise;
        return pari;
    }

    public void print() {
        System.out.println(name + ":Area : " + circleArea());
        System.out.println(name + ":Perimeter : " + circlePerimeter());
    }
}


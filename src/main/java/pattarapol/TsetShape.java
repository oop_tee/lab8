package pattarapol;

public class TsetShape {
    public static void main(String[] args) {
        Rectangle rect1 = new Rectangle("rect1",10,5);
        rect1.getName();
        rect1.calArea();
        rect1.print();
        rect1.printPerimeter();
        Rectangle rect2 = new Rectangle("rect2",5,3);
        rect2.getName();
        rect2.calArea();
        rect2.print();
        rect2.printPerimeter();
        Circle circle1 = new Circle("circle1",1);
        circle1.print();
        Circle circle2 = new Circle("circle2",2);
        circle2.print();
        Triangle triangle = new Triangle("triangle",5,5,6);
        triangle.print();
    }
}
